<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Core\ValueObject\PhpVersion;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    // Define paths
    $rectorConfig->paths([
        __DIR__.'/src',
    ]);

    // Define PHP version
    $rectorConfig->phpVersion(PhpVersion::PHP_82);

    // Define sets of rules
    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_82,
    ]);
};
